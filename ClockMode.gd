extends Node2D

const CENTER_RADIUS = 6

var font = DynamicFont.new()

# Draws a clock
func _draw():
	var clock = get_parent().get_parent()
	
	font.font_data = load("res://fonts/Esteban-Regular.ttf")
	font.size = clock.digit_size
	var viewportSize = get_viewport_rect().size
	var center = Vector2(viewportSize.x / 2, viewportSize.y / 2)
	
	# draw digits
	var margin = max(50, center.y * 0.3)
	var distanceNumberToCenter = max(0, center.y - margin)
	
	for i in range(1, 13):
		var digitAngle = deg2rad(-30 * i + 180)
		var digitVectorX = center.x + (distanceNumberToCenter * sin(digitAngle))
		var digitVectorY = center.y + (distanceNumberToCenter * cos(digitAngle))
		var offset = 0
		if i >= 10:
			offset = clock.digit_size / 2.0
		else:
			offset = clock.digit_size / 4.0
		draw_string(font, Vector2(digitVectorX - offset, digitVectorY + offset), str(i), clock.digit_color)
	
	# draw clock hands
	var timeDict = OS.get_time();
	var hours = float(timeDict.hour);
	var minutes = float(timeDict.minute);
	var seconds = float(timeDict.second);
	
	# hours
	hours += minutes / 60
	var hourHandLength = max(CENTER_RADIUS + 1, distanceNumberToCenter - clock.digit_size * 2)
	var hourHandAngle = deg2rad(-30 * hours + 180)
	var hourHandVectorX = center.x + (hourHandLength * sin(hourHandAngle))
	var hourHandVectorY = center.y + (hourHandLength * cos(hourHandAngle))
	draw_line(center, Vector2(hourHandVectorX, hourHandVectorY), clock.hour_hand_color, 6)
	
	# minutes
	minutes += (floor(seconds / 5) * 5) / 60
	var minuteHandLength = max(CENTER_RADIUS + 1, distanceNumberToCenter - clock.digit_size * 1.25)
	var minuteHandAngle = deg2rad(-6 * minutes + 180)
	var minuteHandVectorX = center.x + (minuteHandLength * sin(minuteHandAngle))
	var minuteHandVectorY = center.y + (minuteHandLength * cos(minuteHandAngle))
	draw_line(center, Vector2(minuteHandVectorX, minuteHandVectorY), clock.minute_hand_color, 4)
	
	# seconds
	if clock.show_seconds:
		var secondHandLength = max(CENTER_RADIUS + 1, distanceNumberToCenter - clock.digit_size * 0.75)
		var secondHandAngle = deg2rad(-6 * seconds + 180)
		var secondHandVectorX = center.x + (secondHandLength * sin(secondHandAngle))
		var secondHandVectorY = center.y + (secondHandLength * cos(secondHandAngle))
		draw_line(center, Vector2(secondHandVectorX, secondHandVectorY), clock.second_hand_color, 2)
	
	# draw center
	draw_circle(center, CENTER_RADIUS, Color.black)
