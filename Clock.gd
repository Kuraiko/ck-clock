extends Node2D

const SETTING_FILE_PATH = "user://ck_clock.settings"
const FONT_PATH = "res://fonts/Esteban-Regular.ttf"
const FONT_NAME = "Esteban-Regular"

# Default settings
const DEFAULT_LANGUAGE = "de"
const DEFAULT_FONT_SIZE = 32
const DEFAULT_DIGIT_SIZE = 24
const DEFAULT_SHOW_SECONDS = true
const DEFAULT_AM_PM_ENABLED = false
const DEFAULT_TEXT_COLOR = "ffffffff"  # Color.white
const DEFAULT_DIGIT_COLOR = "ffffffff"  # Color.white
const DEFAULT_HOUR_HAND_COLOR = "ffffffff"  # Color.white
const DEFAULT_MINUTE_HAND_COLOR = "ffbfbfbf"  # Color.gray
const DEFAULT_SECOND_HAND_COLOR = "ff8c0000"  # Color.darkred
const DEFAULT_BACKGROUND_COLOR = "ff0d0b0b"
const DEFAULT_BACKGROUND_PICTURE_PATH = ""
const DEFAULT_TEXT_MODE = true
const DEFAULT_WINDOW_SIZE_X = 200
const DEFAULT_WINDOW_SIZE_Y = 200
const DEFAULT_WINDOW_POSITION_X = 0
const DEFAULT_WINDOW_POSITION_Y = 0

const LANGUAGES = ["DE", "EN"]

var hours = 0
var minutes = 0
var seconds = 0

var options_visible = false

# Settings for option container
var language = DEFAULT_LANGUAGE
var font_size = DEFAULT_FONT_SIZE
var digit_size = DEFAULT_DIGIT_SIZE
var show_seconds = DEFAULT_SHOW_SECONDS
var am_pm_enabled = DEFAULT_AM_PM_ENABLED
var text_color = DEFAULT_TEXT_COLOR
var digit_color = DEFAULT_DIGIT_COLOR
var hour_hand_color = DEFAULT_HOUR_HAND_COLOR
var minute_hand_color = DEFAULT_MINUTE_HAND_COLOR
var second_hand_color = DEFAULT_SECOND_HAND_COLOR
var background_color = DEFAULT_BACKGROUND_COLOR
var background_picture_path = DEFAULT_BACKGROUND_PICTURE_PATH

# Settings for window state
# These are not accesible by the option container
var text_mode = DEFAULT_TEXT_MODE
var window_size_x = DEFAULT_WINDOW_SIZE_X
var window_size_y = DEFAULT_WINDOW_SIZE_Y
var window_position_x = DEFAULT_WINDOW_POSITION_X
var window_position_y = DEFAULT_WINDOW_POSITION_Y

# Initialization
func _ready():
	get_tree().set_auto_accept_quit(false)
	load_settings()
	get_tree().get_root().connect("size_changed", self, "handle_size_changed")
	$Control/OptionsContainer.hide()
	$Control/OptionsBackground.hide()
	$Control/BackgroundPicture.show()
	addOkButtonsToColorPickers()

func addOkButtonsToColorPickers():
	var color_picker_buttons = get_tree().get_nodes_in_group("ColorPickers")
	for color_picker_button in color_picker_buttons:
		var color_picker_popup = color_picker_button.get_popup()
		var color_picker = color_picker_popup.get_child(0)
		var h_seperator = HSeparator.new()
		color_picker.add_child(h_seperator)
		var v_box = VBoxContainer.new()
		var ok_button = Button.new()
		ok_button.text = TranslationServer.translate("BUTTON_OK")
		ok_button.connect("pressed", self, "_on_ButtonColorPickerOk_pressed", [color_picker_popup])
		v_box.add_child(ok_button)
		color_picker.add_child(v_box)

# Checks for every frame, which mode is visible and renders it
# warning-ignore:unused_argument
func _process(delta):
	if not options_visible:
		if text_mode:
			$Control/CenterContainer/TextMode.show()
			$Control/ClockMode.hide()
			showTimeText()
		else:
			$Control/CenterContainer/TextMode.hide()
			$Control/ClockMode.show()
			showTimeClock()

# Assembles text for the time label in clock mode
func showTimeText():
	var timeDict = OS.get_time();
	hours = timeDict.hour;
	minutes = timeDict.minute;
	seconds = timeDict.second;
	var am_pm_hours = hours % 12
	if am_pm_hours == 0:
		am_pm_hours = 12
	var am_pm_text = "a.m." if hours < 12 else "p.m."
	if show_seconds:
		if am_pm_enabled:
			$Control/CenterContainer/TextMode/Time.text = "%02d:%02d:%02d %s" % [am_pm_hours, minutes, seconds, am_pm_text]
		else:
			$Control/CenterContainer/TextMode/Time.text = "%02d:%02d:%02d" % [hours, minutes, seconds]
	else:
		if am_pm_enabled:
			$Control/CenterContainer/TextMode/Time.text = "%02d:%02d %s" % [am_pm_hours, minutes, am_pm_text]
		else:
			$Control/CenterContainer/TextMode/Time.text = "%02d:%02d" % [hours, minutes]

# Updates the clock in clock mode
func showTimeClock():
	$Control/ClockMode.update()

# Switches between modes
func _on_SwitchModeButton_pressed():
	text_mode = not text_mode
	save_settings()

# Function to handle all size changes that godot doesn't update automatically
func handle_size_changed():
	window_size_x = get_viewport_rect().size.x
	window_size_y = get_viewport_rect().size.y
	$Control.rect_size = get_viewport_rect().size
	$Control/OptionsContainer.rect_size = Vector2(get_viewport_rect().size.x, get_viewport_rect().size.y - 50)
	$Control/FileDialog.rect_size = Vector2(get_viewport_rect().size.x - 20, get_viewport_rect().size.y - 20)

# Controls which objects are visible when the option button is toggled
func _on_Options_pressed():
	options_visible = not options_visible
	if options_visible:
		$Control/OptionsContainer.show()
		$Control/OptionsBackground.show()
		$Control/BackgroundPicture.hide()
		$Control/CenterContainer/TextMode.hide()
		$Control/ClockMode.hide()
		$Control/SwitchMode.hide()
	else:
		$Control/OptionsContainer.hide()
		$Control/OptionsBackground.hide()
		$Control/BackgroundPicture.show()
		$Control/SwitchMode.show()
		save_settings()

# Saves settings to SETTING_FILE_PATH
func save_settings():
	var save_dict = {
	"language": language,
	"font_size": font_size,
	"digit_size": digit_size,
	"show_seconds": show_seconds,
	"am_pm_enabled": am_pm_enabled,
	"text_color": text_color,
	"digit_color": digit_color,
	"hour_hand_color": hour_hand_color,
	"minute_hand_color": minute_hand_color,
	"second_hand_color": second_hand_color,
	"background_color": background_color,
	"background_picture_path": background_picture_path,
	"text_mode": text_mode,
	"window_size_x": window_size_x,
	"window_size_y": window_size_y,
	"window_position_x": window_position_x,
	"window_position_y": window_position_y
	}
	var settings_file = File.new()
	settings_file.open(SETTING_FILE_PATH, File.WRITE)
	settings_file.store_line(to_json(save_dict))
	settings_file.close()

# Loads settings from SETTING_FILE_PATH
func load_settings():
	var settings_file = File.new()
	if settings_file.file_exists(SETTING_FILE_PATH):
		settings_file.open(SETTING_FILE_PATH, File.READ)
		var settings_dict = parse_json(settings_file.get_line())
		for key in settings_dict.keys():
			set(key, settings_dict[key])
	settings_file.close()

	prepare_options()
	prepare_options_container()

# Some options won't come straight forward into action like the window size.
# In this function those options are set.
func prepare_options():
	OS.set_window_size(Vector2(window_size_x, window_size_y))
	OS.set_window_position(Vector2(window_position_x, window_position_y))
	TranslationServer.set_locale(language)
	set_text_mode_font()
	set_text_color()
	set_background_color()
	set_background_picture()

# Makes sure that every option in the option container is shown accordingly to the loaded settings.
func prepare_options_container():
	$Control/OptionsContainer/VBoxContainer/ShowSecondsContainer/CheckBoxShowSeconds.pressed = show_seconds
	$Control/OptionsContainer/VBoxContainer/EnableAMPMModeContainer/CheckBoxAMPM.pressed = am_pm_enabled
	$Control/OptionsContainer/VBoxContainer/EnableAMPMModeContainer/Checkbox24h.pressed = not am_pm_enabled
	$Control/OptionsContainer/VBoxContainer/FontSizeTextModeContainer/SpinBoxFontSize.value = font_size
	$Control/OptionsContainer/VBoxContainer/FontSizeClockModeContainer/SpinBoxDigitSize.value = digit_size
	$Control/OptionsContainer/VBoxContainer/TextColorContainer/ColorPickerForText.color = text_color
	$Control/OptionsContainer/VBoxContainer/DigitColorContainer/ColorPickerForDigits.color = digit_color
	$Control/OptionsContainer/VBoxContainer/HourHandColorContainer/ColorPickerForHourHand.color = hour_hand_color
	$Control/OptionsContainer/VBoxContainer/MinuteHandColorContainer/ColorPickerForMinuteHand.color = minute_hand_color
	$Control/OptionsContainer/VBoxContainer/SecondHandColorContainer/ColorPickerForSecondHand.color = second_hand_color
	$Control/OptionsContainer/VBoxContainer/BackgroundColorContainer/ColorPickerForBackground.color = background_color
	
	var dropdown_language = $Control/OptionsContainer/VBoxContainer/LanguageContainer/DropdownLanguage
	for lang in LANGUAGES:
		dropdown_language.add_item(lang)
	var current_language_id = LANGUAGES.find(language.to_upper())
	dropdown_language.select(current_language_id)

# Makes sure that all settings are saved when the app is closed
func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		window_position_x = OS.window_position.x
		window_position_y = OS.window_position.y
		save_settings()
		get_tree().quit()

# Restores default settings of the options container
func _on_RestoreDefaultButton_pressed():
	language = DEFAULT_LANGUAGE
	font_size = DEFAULT_FONT_SIZE
	digit_size = DEFAULT_DIGIT_SIZE
	show_seconds = DEFAULT_SHOW_SECONDS
	am_pm_enabled = DEFAULT_AM_PM_ENABLED
	text_color = DEFAULT_TEXT_COLOR
	digit_color = DEFAULT_DIGIT_COLOR
	hour_hand_color = DEFAULT_HOUR_HAND_COLOR
	minute_hand_color = DEFAULT_MINUTE_HAND_COLOR
	second_hand_color = DEFAULT_SECOND_HAND_COLOR
	background_color = DEFAULT_BACKGROUND_COLOR
	background_picture_path = DEFAULT_BACKGROUND_PICTURE_PATH
	
	prepare_options()
	prepare_options_container()
	save_settings()

# Restores default settings which are not set by the options container
func _on_RestoreWindowButton_pressed():
	text_mode = DEFAULT_TEXT_MODE
	window_size_x = DEFAULT_WINDOW_SIZE_X
	window_size_y = DEFAULT_WINDOW_SIZE_Y
	window_position_x = DEFAULT_WINDOW_POSITION_X
	window_position_y = DEFAULT_WINDOW_POSITION_Y
	
	prepare_options()
	save_settings()

# Stores value of CheckBoxShowSeconds
func _on_CheckBoxShowSeconds_toggled(button_pressed):
	show_seconds = button_pressed

# Stores value of Checkbox24h
func _on_Checkbox24h_toggled(button_pressed):
	am_pm_enabled = not button_pressed
	$Control/OptionsContainer/VBoxContainer/EnableAMPMModeContainer/CheckBoxAMPM.pressed = am_pm_enabled

# Stores value of CheckBoxAMPM
func _on_CheckBoxAMPM_toggled(button_pressed):
	am_pm_enabled = button_pressed
	$Control/OptionsContainer/VBoxContainer/EnableAMPMModeContainer/Checkbox24h.pressed = not am_pm_enabled

# Stores selected value of DropdownLanguage and sets current language
func _on_DropdownLanguage_item_selected(id):
	var dropdown_language = $Control/OptionsContainer/VBoxContainer/LanguageContainer/DropdownLanguage
	language = dropdown_language.get_item_text(id).to_lower()
	TranslationServer.set_locale(language)

# Stores value of SpinBoxFontSize and sets text mode font size
func _on_SpinBoxFontSize_value_changed(value):
	font_size = value
	set_text_mode_font()

# Stores value of SpinBoxDigitSize
func _on_SpinBoxDigitSize_value_changed(value):
	digit_size = value

# Sets text mode font size
func set_text_mode_font():
	var font = DynamicFont.new()
	font.font_data = load(FONT_PATH)
	font.size = font_size
	$Control/CenterContainer/TextMode/Time.set("custom_fonts/font", font)

# Opens a file dialog
func _on_ButtonOpenFileDialog_pressed():
	$Control/FileDialog.rect_size = Vector2(get_viewport_rect().size.x - 20, get_viewport_rect().size.y - 20)
	$Control/FileDialog.popup()

func _on_FileDialog_file_selected(path):
	background_picture_path = path
	set_background_picture()

# Sets the background picture to the background_picture_path
# If the path is not valid, the background picture will be emptied
func set_background_picture():
	if background_picture_path != "":
		var image_texture = ImageTexture.new()
		var error = image_texture.load(background_picture_path)
		if error != OK:
			print("Error " + str(error))
			background_picture_path = ""
			$Control/BackgroundPicture.texture = null
		else:
			$Control/BackgroundPicture.texture = image_texture
	else:
		$Control/BackgroundPicture.texture = null

# hides the color picker
func _on_ButtonColorPickerOk_pressed(color_picker_popup):
	color_picker_popup.hide()

# converts color to string representation and sets text color
func _on_ColorPickerForText_color_changed(color):
	text_color = color.to_html()
	set_text_color()

# sets text color
func set_text_color():
	$Control/CenterContainer/TextMode/Time.add_color_override("font_color", text_color)

func _on_ColorPickerForDigits_color_changed(color):
	digit_color = color.to_html()

func _on_ColorPickerForHourHand_color_changed(color):
	hour_hand_color = color.to_html()

func _on_ColorPickerForMinuteHand_color_changed(color):
	minute_hand_color = color.to_html()

func _on_ColorPickerForSecondHand_color_changed(color):
	second_hand_color = color.to_html()

func _on_ColorPickerForBackground_color_changed(color):
	background_color = color.to_html()
	set_background_color()

func set_background_color():
	var style_box = StyleBoxFlat.new()
	style_box.bg_color = background_color
	$Control/Background.set("custom_styles/panel", style_box)
