# CK Clock

Just a simple clock.

These project was started to learn the basics of godot while also providing my mum with a clock app.
She said she couldn't find anything fitting (I don't think she searched), so I made her one.

## Getting Started

### Prerequisites

You will need [Godot](https://godotengine.org/) and the [export templates](https://downloads.tuxfamily.org/godotengine/3.1.2/Godot_v3.1.2-stable_export_templates.tpz) for the systems you want to use.

Alternativly you can use [Docker](https://www.docker.com/).

### Installing

#### Using local Godot instance

Install Godot and the export templates (see [this tutorial](https://docs.godotengine.org/en/3.1/getting_started/step_by_step/exporting.html#export-templates) for more information).

#### Using Docker
Either pull the newest Docker Image `registry.gitlab.com/kuraiko/ck-clock:latest` or build the Image yourself with

``` bash
docker build -t name_of_the_image .
```

You can then run the docker container to develop inside it with:

``` bash
docker run -it name_of_the_image bash
```
You can then either just work inside the container with commandline tools, or you can work on your local machine and copy the files into the container with

``` bash
docker cp your_local_file container_name:/path/in/container/file_name
```


### Build the Project

You can build the project on the console with the following command:

``` bash
godot project.godot --export "Windows Desktop" your_project_name.exe
```

Or open your Godot instance and export the project as descriped in [this tutorial](https://docs.godotengine.org/en/3.1/getting_started/step_by_step/exporting.html#exporting-by-platform).

### Running the tests

No tests available yet.

### Running the app

Just run your_project_name.exe

## Deployment

Releases are automatically deployed by the GitLab CI.

## Built With

* [Godot](https://godotengine.org/) - Game engine used to develop the app
* [Docker](https://www.docker.com/) - CI uses Docker

## Contributing

If you want to know how to contribute, read the [Contributing](CONTRIBUTING.md) file

## Authors

* **Corinna Koch** - *Initial work* - @kuraiko

## Credits

* CI and Dockerfile are created and are maintained by @haferf

* Clock icon made by Freepik from www.flaticon.com
* Switch and Gear icon made by bqlqn from www.flaticon.com

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* My mother for her ideas

