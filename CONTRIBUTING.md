# Contributing to CK Clock
This is just a small test project. But if you are interested in contributing anyway, here is how you can help:

- Reporting a bug
- Discussing the current state of the code
- Submitting a fix
- Proposing new features

## I develop with GitLab
I use gitLab to host code, to track issues and feature requests, as well as accept merge requests.

Merge requests are the best way to propose changes:

1. Fork the repo and create your branch from `master`.
2. If you've added code that should be tested, add tests.
3. If you've changed APIs, update the documentation.
4. Ensure the test suite passes.
5. Make sure your code lints.
6. Issue that merge request!

## Any contributions you make will be under the MIT Software License
In short, when you submit code changes, your submissions are understood to be under the same [MIT License](http://choosealicense.com/licenses/mit/) that covers the project. Feel free to contact the maintainers if that's a concern.

## Report bugs using GitLab's [issues](https://gitlab.com/Kuraiko/ck-clock/issues)
I use GitHub issues to track public bugs. Report a bug by [opening a new issue](); it's that easy!

**Great Bug Reports** tend to have:

- A quick summary and/or background
- Steps to reproduce
  - Be specific!
  - Give sample code if you can.
- What you expected would happen
- What actually happens
- Notes (possibly including why you think this might be happening, or stuff you tried that didn't work)

## Coding Style

Sadly, this project doesn't have a consistent coding style (yet?).
We are trying to use the coding style proposed by [Godot](https://docs.godotengine.org/en/3.1/getting_started/scripting/gdscript/gdscript_styleguide.html).

## License
By contributing, you agree that your contributions will be licensed under its MIT License.

## References
This document was adapted from the open-source contribution guidelines for [Facebook's Draft](https://github.com/facebook/draft-js/blob/a9316a723f9e918afde44dea68b5f9f39b7d9b00/CONTRIBUTING.md)
